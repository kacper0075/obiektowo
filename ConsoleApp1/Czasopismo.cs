﻿using System;

namespace ConsoleApp1
{
    public class Czasopismo : Pozycja
    {
        private int numer;

        public Czasopismo()
        {
            this.numer = 0;
        }
      
        public Czasopismo(string tytul, int id, string wydawnictwo, int rokWydania, int numer) : base(tytul, id, wydawnictwo, rokWydania)
        {
            this.numer = numer;
        }

        public override void WypiszInfo()
        {
            Console.WriteLine("---[ Czasopismo ]---");
            Console.WriteLine("Tytuł: " + this.tytul);
            Console.WriteLine("Id: " + this.id);
            Console.WriteLine("Wydawnictwo: " + this.wydawnictwo);
            Console.WriteLine("Rok Wydania: " + this.rokWydania);
            Console.WriteLine("Numer: " + this.numer);
            Console.WriteLine("---[ /Czasopismo ]---");
        }
    }
}