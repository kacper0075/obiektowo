﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    public class Ksiazka : Pozycja
    {
        private int liczbaStron;
        private List<Autor> autorzy = new List<Autor>();

        public Ksiazka()
        {
            this.liczbaStron = 0;
        }
        
        public Ksiazka(string tytul, int id, string wydawnictwo, int rokWydania, int liczbaStron) : base(tytul, id, wydawnictwo, rokWydania)
        {
            this.liczbaStron = liczbaStron;
        }

        public void DodajAutora(Autor autor)
        {
            this.autorzy.Add(autor);
        }
        
        public override void WypiszInfo()
        {
            Console.WriteLine("---[ Książka ]---");
            Console.WriteLine("Tytuł: " + this.tytul);
            Console.WriteLine("Id: " + this.id);
            Console.WriteLine("Wydawnictwo: " + this.wydawnictwo);
            Console.WriteLine("Rok Wydania: " + this.rokWydania);
            Console.WriteLine("Liczba Stron: " + this.liczbaStron);
            if (this.autorzy.Count != 0)
            {
                Console.WriteLine("---[ Autorzy ]---");
                foreach (var autor in this.autorzy)
                {
                    Console.WriteLine("Imie: " + autor.Imie);
                    Console.WriteLine("Nazwisko: " + autor.Nazwisko);
                    Console.WriteLine("Narodowosc: " + autor.Narodowosc);
                }
                Console.WriteLine("---[ /Autorzy ]---");
            }
            Console.WriteLine("---[ /Książka ]---");
        }
    }
}