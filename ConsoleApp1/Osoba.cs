﻿namespace ConsoleApp1
{
    public abstract class Osoba
    {
        protected string imie;
        protected string nazwisko;

        public string Imie => imie;
        public string Nazwisko => nazwisko;

        protected Osoba()
        {
            this.imie = "";
            this.nazwisko = "";
        }

        protected Osoba(string imie, string nazwisko)
        {
            this.imie = imie;
            this.nazwisko = nazwisko;
        }
    }
}