﻿namespace ConsoleApp1
{
    public class Bibliotekarz : Osoba
    {
        private string dataZatrudnienia;
        private double wynagrodzenie;

        public string DataZatrudnienia => dataZatrudnienia;
        public double Wynagrodzenie => wynagrodzenie;

        public Bibliotekarz()
        {
        }

        public Bibliotekarz(string imie, string nazwisko, string dataZatrudnienia, double wynagrodzenie) : base(imie, nazwisko)
        {
            this.dataZatrudnienia = dataZatrudnienia;
            this.wynagrodzenie = wynagrodzenie;
        }
    }
}