﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    public class Biblioteka : IZarzadzaniePozycjami
    {
        private string adres;
        public string Adres => adres;
        
        private List<Bibliotekarz> bibliotekarze = new List<Bibliotekarz>();
        private List<Katalog> katalogi = new List<Katalog>();

        public Biblioteka()
        {
        }

        public Biblioteka(string adres)
        {
            this.adres = adres;
        }

        public void DodajBibliotekarza(Bibliotekarz bibliotekarz)
        {
            this.bibliotekarze.Add(bibliotekarz);
        }

        public void WypiszBibliotekarzy()
        {
            Console.WriteLine("---[ Bibliotekarze ]---");
            foreach (var bibliotekarz in this.bibliotekarze)
            {
                Console.WriteLine("Imie: " + bibliotekarz.Imie);
                Console.WriteLine("Nazwisko: " + bibliotekarz.Nazwisko);
                Console.WriteLine("Wynagrodzenie: " + bibliotekarz.Wynagrodzenie);
                Console.WriteLine("Data Zatrudnienia: " + bibliotekarz.DataZatrudnienia);
            }
            Console.WriteLine("---[ /Bibliotekarze ]---");
        }

        public void DodajKatalog(Katalog katalog)
        {
            this.katalogi.Add(katalog);
        }
        
        public void WypiszKatalogi()
        {
            Console.WriteLine("---[ Katalogi ]---");
            foreach (var katalog in this.katalogi)
            {
                Console.WriteLine(katalog.DzialTematyczny);
            }
            Console.WriteLine("---[ /Katalogi ]---");
        }
        
        public Pozycja ZnajdzPozycjePoTytule(string tytul)
        {
            foreach (var katalog in this.katalogi)
            {
                return katalog.ZnajdzPozycjePoTytule(tytul);
            }
            return null;
        }

        public Pozycja ZnajdzPozycjePoId(int id)
        {
            foreach (var katalog in this.katalogi)
            {
                return katalog.ZnajdzPozycjePoId(id);
            }
            return null;
        }

        public void WypiszWszystkiePozycje()
        {
            foreach (var katalog in this.katalogi)
            {
                katalog.WypiszWszystkiePozycje();
            }
        }

        public void DodajPozycje(Pozycja pozycja, string dzialTematyczny)
        {
            foreach (var katalog in this.katalogi)
            {
                if (katalog.DzialTematyczny == dzialTematyczny)
                {
                    katalog.DodajPozycje(pozycja);
                }
            }
            
        }
        
    }
}