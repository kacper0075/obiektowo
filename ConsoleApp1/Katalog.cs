﻿using System.Collections.Generic;

namespace ConsoleApp1
{
    public class Katalog : IZarzadzaniePozycjami
    {
        private string dzialTematyczny;
        private List<Pozycja> pozycje = new List<Pozycja>();

        public string DzialTematyczny => dzialTematyczny;

        public Katalog()
        {
            this.dzialTematyczny = "";
        }

        public Katalog(string dzialTematyczny)
        {
            this.dzialTematyczny = dzialTematyczny;
        }

        public void DodajPozycje(Pozycja pozycja)
        {
            this.pozycje.Add(pozycja);
        }

        public Pozycja ZnajdzPozycje(string tytul, int id, string wydawnictwo, int rokWydania)
        {
            foreach (var pozycja in this.pozycje)
            {
                if (pozycja.Tytul == tytul && pozycja.Id == id && pozycja.Wydawnictwo == wydawnictwo && pozycja.RokWydania == rokWydania)
                {
                    return pozycja;
                }
            }
            return null;
        }

        public Pozycja ZnajdzPozycjePoTytule(string tytul)
        {
            foreach (var pozycja in this.pozycje)
            {
                if (pozycja.Tytul == tytul)
                {
                    return pozycja;
                }
            }
            return null;
        }

        public Pozycja ZnajdzPozycjePoId(int id)
        {
            foreach (var pozycja in this.pozycje)
            {
                if (pozycja.Id == id)
                {
                    return pozycja;
                }
            }
            return null;
        }

        public void WypiszWszystkiePozycje()
        {
            foreach (var pozycja in this.pozycje)
            {
                pozycja.WypiszInfo();
            }
        }
    }
}