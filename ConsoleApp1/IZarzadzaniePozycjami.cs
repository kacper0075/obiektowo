﻿namespace ConsoleApp1
{
    public interface IZarzadzaniePozycjami
    {
        Pozycja ZnajdzPozycjePoTytule(string tytul);
        Pozycja ZnajdzPozycjePoId(int id);
        void WypiszWszystkiePozycje();
    }
}