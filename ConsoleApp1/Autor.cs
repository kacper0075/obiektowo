﻿namespace ConsoleApp1
{
    public class Autor : Osoba
    {
        private string narodowosc;

        public string Narodowosc => narodowosc;

        public Autor()
        {
            this.narodowosc = "";
        }
        
        public Autor(string imie, string nazwisko, string narodowosc) : base(imie, nazwisko)
        {
            this.narodowosc = narodowosc;
        }

    }
}