﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var bi = new Biblioteka("WSB Gdańsk");
            
            var ka = new Katalog("Szkoła");
            var ka2 = new Katalog("Safe");

            var ks1 = new Ksiazka("k1", 1, "WSB", 2132, 360);
            var ks2 = new Ksiazka("k1", 1, "WSB", 2013, 360);
            var cz1 = new Czasopismo("c1", 1, "WSB", 2023, 1);
            var cz2 = new Czasopismo("c1", 1, "WSB", 2023, 1);
            
            var bb = new Bibliotekarz("Jacek", "Thomson", "20.05.2016", 5000);
            
            var au1 = new Autor("Jan", "Kowalski", "Polak");
            var au2 = new Autor("Jacek", "Kowalski", "Rusek");
            
            bi.DodajBibliotekarza(bb);
            bi.DodajKatalog(ka);
            bi.DodajKatalog(ka2);
            bi.DodajPozycje(ks2, "Safe");
            bi.DodajPozycje(cz2, "Safe");

            ks1.DodajAutora(au1);
            ks2.DodajAutora(au2);
            
            ka.DodajPozycje(ks1);
            ka.DodajPozycje(cz1);
            
            bi.WypiszBibliotekarzy();
            bi.WypiszKatalogi();
            bi.WypiszWszystkiePozycje();
        }
    }
}