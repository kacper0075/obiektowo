﻿namespace ConsoleApp1
{
    public abstract class Pozycja : Katalog
    {
        protected string tytul = "";
        protected int id = 0;
        protected string wydawnictwo = "";
        protected int rokWydania = 0;

        public string Tytul => tytul;
        public int Id => id;
        public string Wydawnictwo => wydawnictwo;
        public int RokWydania => rokWydania;

        public Pozycja()
        {
            this.tytul = "";
            this.id = 0;
            this.wydawnictwo = "";
            this.rokWydania = 0;
        }

        public Pozycja(string tytul, int id, string wydawnictwo, int rokWydania)
        {
            this.tytul = tytul;
            this.id = id;
            this.wydawnictwo = wydawnictwo;
            this.rokWydania = rokWydania;
        }

        public abstract void WypiszInfo();
    }
}